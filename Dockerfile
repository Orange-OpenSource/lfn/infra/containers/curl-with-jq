FROM alpine:3.15

LABEL org.opencontainers.image.authors="Sylvain Desbureaux <sylvain.desbureaux@orange.com>" \
    org.opencontainers.image.licenses="Apache-2.0" \
    org.opencontainers.image.base.name="docker.io/library/alpine:3.15"

ARG user=onap
ARG group=onap

RUN apk add --no-cache \
    curl=~7.80 \
    grep=~3.7 \
    jq=~1.6

RUN addgroup -S $group && adduser -G $group -D $user && \
    mkdir /app && \
    chown -R $user:$group /app

# Tell docker that all future commands should be run as the onap user
USER $user
WORKDIR /app

ENTRYPOINT ["sh"]
